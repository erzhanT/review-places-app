const mongoose = require("mongoose");

const ReviewSchema = new mongoose.Schema({
    from: {
        type: mongoose.Schema.Types.ObjectID,
        ref: "User",
        required: true
    },
    fromName: String,
    datetime: Date,
    text: {
        type: String,
        required: true
    },
    food: {
        type: Number,
        required: true
    },
    interior: {
        type: Number,
        required: true,
    },
    service: {
        type: Number,
        required: true,
    }
});

const ImageSchema = new mongoose.Schema({
    from: {
        type: mongoose.Schema.Types.ObjectID,
        ref: "User",
        required: true
    },
    image: {
        type: String,
        required: true
    }
});

const PlaceSchema = new mongoose.Schema({
    name: {
        type: String,
        required: true
    },
    description: {
        type: String,
        required: true,
    },
    image: {
        type: String,
        required: true
    },
    images: [ImageSchema],
    reviews: [ReviewSchema],
    user: {
        type: mongoose.Schema.Types.ObjectID,
        ref: "User",
        required: true
    }
});

const Place = mongoose.model("Place", PlaceSchema);
module.exports = Place;