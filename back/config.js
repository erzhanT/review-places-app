const path = require('path');
const rootPath = __dirname;



module.exports = {
    rootPath,
    uploadPath: path.join(rootPath, 'public/uploads'),
    db: {
        url: 'mongodb://localhost/places',
        options: {
            useNewUrlParser: true,
            useUnifiedTopology: true,
            useCreateIndex: true,
        },
    },
    facebook: {
        appId: '2883572618582644',
        appSecret: '38abfae3186028619e622a7e04ac1803',
    }
};