const express = require('express');
const config = require("../config");
const auth = require('../middleware/auth');
const permit = require("../middleware/permit");
const Place = require("../models/Place");
const multer = require("multer");
const path = require("path");
const {nanoid} = require("nanoid");



const router = express.Router();

const storage = multer.diskStorage({
    destination: (req, file, cb) => {
        cb(null,  path.join(config.uploadPath));
    },
    filename: (req, file, cb) => {
        cb(null, nanoid() + path.extname(file.originalname));
    }
});

const upload = multer({storage});



router.get('/',  async (req, res) => {
    const result = await Place.find();
    if (result) {
        res.send(result);
    } else {
        res.sendStatus(404);
    }
});

router.get('/:id', async (req, res) => {
    const result = await Place.findById(req.params.id);
    if (result) {
        res.send(result);
    } else {
        res.sendStatus(404);
    }
});

router.post('/', auth, upload.single("image"), async (req, res) => {
    const placeData = req.body;
    placeData.user = req.user._id;
    if (req.file) {
       placeData.image = req.file.filename;
    }
    const place = new Place(placeData);
    if (req.body.agree === 'false') {
        return res.status(400).send({message: 'You must accept terms'});
    }
    try {
        await place.save();
        res.send(place);
    } catch (e) {
        res.status(400).send(e);
    }

});

router.delete('/:id', auth, permit("admin"), async (req, res) => {
    const result = await Place.findByIdAndRemove({_id: req.params.id});
    if (result) {
        res.send("Place removed");
    } else {
        res.sendStatus(404);
    }
});

module.exports = router;