const express = require('express');
const router = express.Router();
const config = require("../config");
const Place = require("../models/Place");
const auth = require('../middleware/auth');
const permit = require("../middleware/permit");
const multer = require("multer");
const path = require("path");
const {nanoid} = require("nanoid");


const storage = multer.diskStorage({
    destination: (req, file, cb) => {
        cb(null,  path.join(config.uploadPath));
    },
    filename: (req, file, cb) => {
        cb(null, nanoid() + path.extname(file.originalname));
    }
});
const upload = multer({storage});

router.post("/", auth, async (req, res) => {
    const place = await Place.findById(req.body.to);
    const reviewData = req.body;
    reviewData.datetime = new Date();
    try {
        await place.updateOne({$push: {reviews: reviewData}});
        res.send({message: 'success'});
    } catch (e) {
        res.status(400).send(e);
    }
});

router.post("/photo", auth, upload.single("image"), async (req, res) => {
    const place = await Place.findById(req.body.to);
    let imageData = req.body;
    if (req.file) {
        imageData.image = req.file.filename;
    }
    try {
        await place.updateOne({$push: {images: imageData}});
        res.send({message: 'success'});
    } catch (e) {
        res.status(400).send(e);
    }

});

router.put("/:id", auth, permit("admin"), async (req, res) => {
    const place = await Place.findById(req.body.placeId);

    try {
        await place.updateOne({$pull: {reviews: {_id: req.params.id}}});
        res.send({message: "Success"});
    } catch (e) {
        res.status(400).send(e);
    }

});

router.put("/photo/:id", auth, permit("admin"), async (req, res) => {
    const place = await Place.findById(req.body.placeId);

    try {
        await place.updateOne({$pull: {images: {_id: req.params.id}}});
        res.send({message: "Success"});
    } catch (e) {
        res.status(400).send(e);
    }
});

module.exports = router;