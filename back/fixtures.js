const mongoose = require('mongoose');
const config = require('./config');
const User = require("./models/User");
const {nanoid} = require('nanoid');
const Place = require("./models/Place");

const run = async () => {
    await mongoose.connect(config.db.url, config.db.options);

    const collections = await mongoose.connection.db.listCollections().toArray();

    for (const coll of collections) {
        await mongoose.connection.db.dropCollection(coll.name);
    }
    const [user, admin] = await User.create({
        email: "qwer@qwer.qwer",
        password: "312",
        token: nanoid(),
        role: "user",
        displayName: "Moprhling",

    }, {
        email: "zxc@zxc.zxc",
        password: "123",
        token: nanoid(),
        role: "admin",
        displayName: "Shadow",
        avatarImage: "avatar.png"
    });

    const [NoName, Friends, Relax] = await Place.create({
        name: "No Name",
        image: "NoName.jpeg",
        user: user._id,
        images: [{image: 'Relax.jpeg', from: user._id}, {image: 'Friends.jpeg', from: user._id}, {
            image: 'NoName.jpeg',
            from: user._id
        },],
        reviews: [{
            from: user._id,
            fromName: user.displayName,
            text: 'Mattis nunc sed blandit libero. Porttitor eget dolor morbi non arcu. Blandit aliquam etiam erat velit scelerisque in dictum non consectetur. Viverra nibh cras pulvinar mattis. Eu volutpat odio facilisis mauris',
            datetime: '2021-07-28T15:38:25.351+00:00',
            food: 5,
            interior: 4,
            service: 3
        },
            {
                from: user._id,
                fromName: user.displayName,
                text: 'You can display a label on hover to help users pick the correct rating value. The demo uses the onChangeActive prop.',
                datetime: '2021-08-10T15:38:25.351+00:00',
                food: 2,
                interior: 5,
                service: 4
            }],
        description: 'Mattis nunc sed blandit libero. Porttitor eget dolor morbi non arcu. Blandit aliquam etiam erat velit scelerisque in dictum non consectetur. Viverra nibh cras pulvinar mattis. Eu volutpat odio facilisis mauris'
    }, {
        name: "Friends Coffee",
        image: "Friends.jpeg",
        user: user._id,
        images: [{image: 'Relax.jpeg', from: user._id}, {image: 'NoName.jpeg', from: user._id}, {
            image: 'Friends.jpeg',
            from: user._id
        },],
        reviews: [],
        description: 'There is a full range of bar drinks including a wide selection of spirits, Whisky, Brandy and Liqueurs, soft drinks and  beer including and Falkland Islands only real ale.  The wine list includes a variety and styles of wine including the famous Montes premium wine produced in Chile, with wines by the bottle and glass.'
    }, {
        name: "River Pub",
        image: "Relax.jpeg",
        user: admin._id,
        images: [{image: 'NoName.jpeg', from: user._id}, {image: 'Friends.jpeg', from: user._id},],
        reviews: [{
            from: user._id,
            fromName: user.displayName,
            text: 'You can display a label on hover to help users pick the correct rating value. The demo uses the onChangeActive prop.',
            food: 5,
            datetime: '2021-08-11T15:38:25.351+00:00',
            interior: 4,
            service: 3
        },
            {
                from: user._id,
                fromName: user.displayName,
                text: 'You can display a label on hover to help users pick the correct rating value. The demo uses the onChangeActive prop.',
                food: 3,
                datetime: '2021-08-13T15:38:25.351+00:00',
                interior: 4,
                service: 1
            },
            {
                from: user._id,
                fromName: user.displayName,
                text: 'You can display a label on hover to help users pick the correct rating value. The demo uses the onChangeActive prop.',
                food: 5,
                datetime: '2021-08-14T15:38:25.351+00:00',
                interior: 5,
                service: 5
            }],
        description: 'Mattis nunc sed blandit libero. Porttitor eget dolor morbi non arcu. Blandit aliquam etiam erat velit scelerisque in dictum non consectetur. Viverra nibh cras pulvinar mattis. Eu volutpat odio facilisis mauris '
    });

    await mongoose.connection.close();

}

run().catch(console.error);