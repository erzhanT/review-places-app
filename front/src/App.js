import React from "react";
import {Redirect, Route, Switch} from "react-router-dom";
import {useSelector} from "react-redux";
import Register from "./containers/Register/Register";
import Login from "./containers/Login/Login";
import Layout from "./components/Layout/Layout";
import MainPage from "./containers/MainPage/MainPage";
import AddPlace from "./containers/AddPlace/AddPlace";
import SinglePlace from "./containers/SinglePlace/SinglePlace";


const ProtectedRoute = ({isAllowed, redirectTo, ...props}) => {
    return isAllowed ?
        <Route {...props} /> :
        <Redirect to={redirectTo}/>;
};

const App = () => {

    const user = useSelector(state => state.users.user);

    return (
        <div className="App">

            <Layout/>
            <ProtectedRoute
                path="/"
                exact
                isAllowed={!user}
            />
            <Switch>
                <Route path="/" exact component={MainPage}/>
                <Route path="/places/:id" exact component={SinglePlace}/>
                <Route path="/register" component={Register}/>
                <Route path="/login" component={Login}/>
                <Route path="/add_place" component={AddPlace}/>
            </Switch>
        </div>
    );
};

export default App;
