import React from 'react';
import {Link} from "react-router-dom";
import {makeStyles} from '@material-ui/core/styles';
import Card from '@material-ui/core/Card';
import CardActionArea from '@material-ui/core/CardActionArea';
import CardContent from '@material-ui/core/CardContent';
import Typography from '@material-ui/core/Typography';
import StarBorderIcon from '@material-ui/icons/StarBorder';
import {Rating} from "@material-ui/lab";
import Grid from "@material-ui/core/Grid";

const useStyles = makeStyles({
    root: {
        maxWidth: 345,
    },
    media: {
        height: 140,
    },
    marginCard: {
        margin: '10px 0 10px'
    },
    img: {
        height: '300px',
        width: '100%'
    },
});

const PlaceCard = (props) => {

    const classes = useStyles();

    return (

            <Grid item xs={12} sm={6} md={4} className={classes.marginCard}>
                <Link to={"/places/" + props.id} size="small" color="primary">
                <Card className={classes.root}>
                    <CardActionArea>
                        <img
                            src={'http://localhost:8000/uploads/' + props.img}
                            alt='place'
                            className={classes.img}
                        />
                        <CardContent>
                            <Typography gutterBottom variant="h5" component="h2">
                                {props.name}
                            </Typography>

                            <Typography variant="body2" color="textSecondary" component="p">
                                <b>{props.length > 0 ? props.length : 0}</b> photos
                            </Typography>
                            <Typography>
                                <Rating
                                    readOnly
                                    name="customized-empty"
                                    value={props.value}
                                    precision={0.5}
                                    emptyIcon={<StarBorderIcon fontSize="inherit"/>}
                                />
                            </Typography>
                        </CardContent>
                    </CardActionArea>
                </Card>
                </Link>
            </Grid>

    );
};

export default PlaceCard;