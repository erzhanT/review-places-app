import {
    ADD_PLACE_FAILURE, ADD_PLACE_SUCCESS,
    DELETE_PLACE_FAILURE,
    DELETE_PLACE_SUCCESS,
    GET_PLACES_FAILURE,
    GET_PLACES_SUCCESS, GET_SINGLE_PLACE_FAILURE, GET_SINGLE_PLACE_SUCCESS
} from "../actions/placeActions";

const initialState = {
    getPlaceError: null,
    deletePlaceError: null,
    addPlaceError: null,
    getSinglePlaceError: null,
    places: [],
    singlePlace: {}
};

const placesReducer = (state = initialState, action) => {
    switch(action.type) {
        case GET_PLACES_SUCCESS:
            return {...state, places: action.data, getPlaceError: null};
        case GET_PLACES_FAILURE:
            return {...state, getPlaceError: action.error};
        case ADD_PLACE_FAILURE:
            return {...state, addPlaceError: action.error};
        case DELETE_PLACE_FAILURE:
            return {...state, deletePlaceError: action.error};
        case DELETE_PLACE_SUCCESS:
            return {...state, deletePlaceError: null};
        case ADD_PLACE_SUCCESS:
            return {...state, addPlaceError: null};
        case GET_SINGLE_PLACE_SUCCESS:
            return {...state, singlePlace: action.data, getSinglePlaceError: null};
        case GET_SINGLE_PLACE_FAILURE:
            return {...state, getSinglePlaceError: action.error};
        default:
            return state;
    }
};

export default placesReducer;