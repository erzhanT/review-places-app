import axios from "../../axiosApi";
import {historyPush} from "./historyActions";


export const ADD_PLACE_SUCCESS = "ADD_PLACE_SUCCESS";
export const ADD_PLACE_FAILURE = "ADD_PLACE_FAILURE";

export const DELETE_PLACE_SUCCESS = "DELETE_PLACE_SUCCESS";
export const DELETE_PLACE_FAILURE = "DELETE_PLACE_FAILURE";

export const GET_PLACES_SUCCESS = "GET_PLACES_SUCCESS";
export const GET_PLACES_FAILURE = "GET_PLACES_FAILURE";

export const GET_SINGLE_PLACE_SUCCESS = "GET_SINGLE_PLACE_SUCCESS";
export const GET_SINGLE_PLACE_FAILURE = "GET_SINGLE_PLACE_FAILURE";


const addPLACESuccess = (data) => ({type: ADD_PLACE_SUCCESS, data});
const addPlaceFailure = error => ({type: ADD_PLACE_FAILURE, error});
const deletePlaceSuccess = id => ({type: DELETE_PLACE_SUCCESS, id});
const deletePlaceFailure = error => ({type: DELETE_PLACE_FAILURE, error});
const getPlacesSuccess = data => ({type: GET_PLACES_SUCCESS, data});
const getPlacesFailure = error => ({type: GET_PLACES_FAILURE, error});
const getSinglePlaceSuccess = data => ({type: GET_SINGLE_PLACE_SUCCESS, data});
const getSinglePlaceFailure = error => ({type: GET_SINGLE_PLACE_FAILURE, error});

export const getPlaces = () => {
    return async dispatch => {
        try {
            const response = await axios.get("/places");
            dispatch(getPlacesSuccess(response.data));
        } catch (e) {
            dispatch(getPlacesFailure(e));
        }
    }
};

export const addPlace = data => {
    return async dispatch => {
        try {
            const response = await axios.post("/places", data);
            dispatch(addPLACESuccess(response.data));
            dispatch(historyPush("/"));
        } catch (e) {
            dispatch(addPlaceFailure(e));
        }
    }
};

export const deletePlace = id => {
    return async dispatch => {
        try {
            await axios.delete("/places/" + id);
            dispatch(deletePlaceSuccess(id));
            dispatch(historyPush("/"));
        } catch (e) {
            dispatch(deletePlaceFailure(e));
        }
    }
};

export const getSinglePlace = id => {
    return async dispatch => {
        try {
            const response = await axios.get("/places/" + id);
            dispatch(getSinglePlaceSuccess(response.data))
        } catch (e) {
            dispatch(getSinglePlaceFailure(e))
        }
    }
}