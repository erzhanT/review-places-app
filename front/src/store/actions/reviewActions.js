import axios from "../../axiosApi";

export const ADD_REVIEW_PHOTO_SUCCESS = "ADD_REVIEW_PHOTO_SUCCESS"
export const ADD_REVIEW_PHOTO_FAILURE = "ADD_REVIEW_PHOTO_FAILURE"

export const ADD_REVIEW_SUCCESS = "ADD_REVIEW_SUCCESS";
export const ADD_REVIEW_FAILURE = "ADD_REVIEW_FAILURE";

export const DELETE_REVIEW_SUCCESS = "DELETE_REVIEW_SUCCESS";
export const DELETE_REVIEW_FAILURE = "DELETE_REVIEW_FAILURE";

export const DELETE_REVIEW_PHOTO_SUCCESS = "DELETE_REVIEW_PHOTO_SUCCESS";
export const DELETE_REVIEW_PHOTO_FAILURE = "DELETE_REVIEW_PHOTO_FAILURE";


const addReviewSuccess = (data) => ({type: ADD_REVIEW_SUCCESS, data});
const addReviewFailure = error => ({type: ADD_REVIEW_FAILURE, error});
const deleteReviewSuccess = id => ({type: DELETE_REVIEW_SUCCESS, id});
const deleteReviewFailure = error => ({type: DELETE_REVIEW_FAILURE, error});
const addReviewPhotoSuccess = data => ({type: ADD_REVIEW_PHOTO_SUCCESS, data});
const addReviewPhotoFailure = error => ({type: ADD_REVIEW_PHOTO_FAILURE, error});
const deleteReviewPhotoSuccess = id => ({type: DELETE_REVIEW_PHOTO_SUCCESS, id});
const deleteReviewPhotoFailure = error => ({type: DELETE_REVIEW_PHOTO_FAILURE, error});

export const addReview = data => {
    return async dispatch => {
        try {
            await axios.post("/reviews", data);
            dispatch(addReviewSuccess());
        } catch (e) {
            dispatch(addReviewFailure(e));
        }
    }
};

export const addReviewPhoto = data => {
    return async dispatch => {
        try {
            await axios.post("/reviews/photo", data);
            dispatch(addReviewPhotoSuccess());
        } catch (e) {
            dispatch(addReviewPhotoFailure(e));
        }
    }
};

export const deleteReview = (id, placeId) => {
    return async dispatch => {
        try {
            await axios.patch("/reviews/" + id, { placeId});
            dispatch(deleteReviewSuccess(id));
        } catch (e) {
            dispatch(deleteReviewFailure(e));
        }
    }
};

export const deleteReviewPhoto = (id, placeId) => {
    return async dispatch => {
        try {
            await axios.patch("/reviews/photo/" + id, { placeId});
            dispatch(deleteReviewPhotoSuccess(id));
        } catch (e) {
            dispatch(deleteReviewPhotoFailure(e));
        }
    }
};