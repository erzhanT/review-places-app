import React, {useEffect, useRef, useState} from 'react';
import {useSelector, useDispatch} from "react-redux";
import {getSinglePlace, deletePlace} from "../../store/actions/placeActions";
import {addReview, addReviewPhoto, deleteReview, deleteReviewPhoto} from "../../store/actions/reviewActions";
import StarBorderIcon from '@material-ui/icons/StarBorder';
import Rating from '@material-ui/lab/Rating';
import TextField from '@material-ui/core/TextField';
import Carousel from 'react-material-ui-carousel'
import {makeStyles} from "@material-ui/core/styles";
import Button from "@material-ui/core/Button";
import {historyPush} from "../../store/actions/historyActions";
// import { AutoRotatingCarousel } from 'material-auto-rotating-carousel';

const useStyles = makeStyles({
    singlePlacePage: {
        width: '1200px',
        margin: '50px auto 0'
    },
    placeInfo: {
        display: 'flex',
        justifyContent: 'space-between',
        borderBottom: '1px solid cadetblue',
        paddingBottom: '20px'
    },
    placePageImage: {
        width: '50%',
        height: '300px'
    },
    reviewForm: {
        padding: '30px',
        borderBottom: '1px solid cadetblue',
        borderTop: '1px solid cadetblue'
    },
    textarea: {
        display: 'block',
        width: '90%',
        resize: 'none',
        height: '100px',
        margin: ' 0 auto 20px 0'
    },
    carouselItem: {
        height: '400px',
        width: 'auto'
    },
    carousel: {
        margin: '0 auto',
    },
    marginButton: {
        margin: '0 0 0 10px'
    },
    placeGallery: {
        borderBottom: '1px solid blue',
        padding: '20px 0',
        display: 'flex',
        justifyContent: 'space-between',
        flexWrap: 'wrap'
    }

});


const SinglePlace = (props) => {
        const user = useSelector(state => state.users.user);
        const singlePlace = useSelector(state => state.places.singlePlace);
        const dispatch = useDispatch();
        const classes = useStyles();

        const [review, setReview] = useState({
            from: user ? user._id : null,
            fromName: user ? user.displayName : null,
            to: props.match.params.id,
            text: '',
            food: 0,
            interior: 0,
            service: 0
        });
        const [image, setImage] = useState({
            to: props.match.params.id,
            image: ''
        });
        const inputRef = useRef();

        useEffect(() => {
            dispatch(getSinglePlace(props.match.params.id));
        }, [dispatch, props.match.params.id]);


        const remove = id => {
            dispatch(deletePlace(id))
        };

        const fileChangeHandler = e => {
            const name = e.target.name;
            const file = e.target.files[0];
            setImage(prevState => ({
                ...prevState, [name]: file
            }));
            historyPush('/');
        };

        const removeReview = async (id) => {
            await dispatch(deleteReview(id, props.match.params.id));
            dispatch(getSinglePlace(props.match.params.id));
        }

        const addRev = data => {
            if (review.text !== '' && review.food !== 0 && review.service !== 0 && review.interior !== 0) {
                dispatch(addReview(data));
                dispatch(getSinglePlace(props.match.params.id));
                window.location.reload();
            }
        }

        const removePhoto = async (id) => {
            await dispatch(deleteReviewPhoto(id, props.match.params.id));
            dispatch(getSinglePlace(props.match.params.id));
        };

        const inputChangeHandler = (e) => {
            const name = e.target.name;
            const value = e.target.value;
            setReview(prevState => {
                return {...prevState, [name]: value};
            });
        };

        const formSubmit = async (e) => {
            e.preventDefault();
            if (image.image !== '') {
                const formData = new FormData();
                Object.keys(image).forEach(key => {
                    formData.append(key, image[key]);
                });
                await dispatch(addReviewPhoto(formData));
                dispatch(getSinglePlace(props.match.params.id));
                setImage(prevState => ({
                    ...prevState, image: ''
                }));
            }
        };
        let totalFood;
        let totalService;
        let totalInterior;
        let total;

        if (singlePlace && singlePlace.reviews) {
            totalFood = singlePlace.reviews.reduce((prev, next) => prev + next.food, 0);
            totalFood = totalFood / singlePlace.reviews.length;
        }
        if (singlePlace && singlePlace.reviews) {
            totalService = singlePlace.reviews.reduce((prev, next) => prev + next.service, 0);
            totalService = totalService / singlePlace.reviews.length;
        }
        if (singlePlace && singlePlace.reviews) {
            totalInterior = singlePlace.reviews.reduce((prev, next) => prev + next.interior, 0);
            totalInterior = totalInterior / singlePlace.reviews.length;
        }
        if (singlePlace && singlePlace.reviews) {
            total = (totalFood + totalInterior + totalService) / 3;
        }

        let gallery;
        if (singlePlace && singlePlace.images && singlePlace.images.length === 0) {
            gallery = (
                <h2>Add first photo...</h2>
            )
        } else if (singlePlace && singlePlace.images) {
            function Item(props) {
                return (
                    <img src={'http://localhost:8000/uploads/' + props.item.image} alt='carousel'
                         className={classes.carouselItem}/>
                )
            }

            gallery = (
                <Carousel className={classes.carousel}
                          animation='fade'
                          indicators={true}
                          autoPlay
                          interval={3000}
                          navButtonsAlwaysInvisible={false}>
                    {
                        singlePlace.images.map((item, i) => <Item key={i} item={item}/>)
                    }
                </Carousel>
            )
        }

        let reviews;
        if (singlePlace && singlePlace.reviews && singlePlace.reviews.length === 0) {
            reviews = (
                <h2>Add first review...</h2>
            )
        } else if (singlePlace && singlePlace.reviews) (
            reviews = singlePlace.reviews.map(review => {
                return (
                    <div key={review._id}>
                        <h4>{review.fromName}</h4>
                        <p>{review.text}</p>
                        <span>Service: </span>
                        <Rating
                            readOnly
                            defaultValue={review.service}
                            precision={0.1}
                            emptyIcon={<StarBorderIcon fontSize="inherit"/>}
                        />
                        <span className='label'>Food: </span>
                        <Rating
                            readOnly
                            defaultValue={review.food}
                            precision={0.1}
                            emptyIcon={<StarBorderIcon fontSize="inherit"/>}
                        />
                        <span className='label'>Interior: </span>
                        <Rating
                            readOnly
                            defaultValue={review.interior}
                            precision={0.1}
                            emptyIcon={<StarBorderIcon fontSize="inherit"/>}
                        />
                        <span>At {new Date(review.datetime).toISOString().slice(0, 10)}</span>
                        <span>{new Date(review.datetime).toISOString().slice(11, 19)}</span>
                        {(user && user.role === 'admin') ?
                            <Button
                                className={classes.marginButton}
                                variant={'contained'}
                                color={'primary'}
                                onClick={() => removeReview(review._id)}
                            >
                                Remove
                            </Button>
                            : null}

                    </div>
                )
            })
        )

        return (
            <div className={classes.singlePlacePage}>
                <div className={classes.placeInfo}>
                    <div>
                        <h1>{singlePlace.name}</h1>
                        <p><b>Description:</b> {singlePlace.description}</p>
                        {(user && user.role === 'admin') ?
                            <Button
                                color={'primary'}
                                variant={'contained'}
                                onClick={() => remove(props.match.params.id)}
                            >
                                Remove
                            </Button>
                            : null}
                    </div>
                    <img src={'http://localhost:8000/uploads/' + singlePlace.image}
                         className={classes.placePageImage} alt='PlacePageImage'/>
                </div>
                <div className={classes.placeGallery}>
                    {gallery}
                </div>
                <div>
                    <h3>Rating</h3>
                    <span>Service: </span>
                    <Rating
                        readOnly
                        value={totalService ? totalService : 0}
                        precision={0.1}
                        emptyIcon={<StarBorderIcon fontSize="inherit"/>}
                    />
                    <span>Food: </span>
                    <Rating
                        readOnly
                        value={totalFood ? totalFood : 0}
                        precision={0.1}
                        emptyIcon={<StarBorderIcon fontSize="inherit"/>}
                    />
                    <span>Interior: </span>
                    <Rating
                        readOnly
                        value={totalInterior ? totalInterior : 0}
                        precision={0.1}
                        emptyIcon={<StarBorderIcon fontSize="inherit"/>}
                    />
                    <h3 style={{marginLeft: '20px', marginTop: '37px'}}>Overall:</h3>

                    <Rating
                        readOnly
                        value={total ? total : 0}
                        precision={0.1}
                        emptyIcon={<StarBorderIcon fontSize="inherit"/>}
                    />
                </div>
                <div>
                    {reviews}
                </div>
                {user ?
                    <div className={classes.reviewForm}>
                        <h3>Add review</h3>

                        <TextField
                            variant="outlined"
                            fullWidth
                            multiline
                            rows={4}
                            required
                            className={classes.textarea}
                            name='text'
                            value={review.text}
                            onChange={inputChangeHandler}
                        />
                        <div>
                            <p>Service: </p>
                            <Rating
                                name="service"
                                defaultValue={review.service}
                                precision={1}
                                onChange={inputChangeHandler}
                                emptyIcon={<StarBorderIcon fontSize="inherit"/>}
                            />
                            <p>Food: </p>
                            <Rating
                                name="food"
                                defaultValue={review.food}
                                precision={1}
                                onChange={inputChangeHandler}
                                emptyIcon={<StarBorderIcon fontSize="inherit"/>}
                            />
                            <p>Interior: </p>
                            <Rating
                                name="interior"
                                defaultValue={review.interior}
                                precision={1}
                                onChange={inputChangeHandler}
                                emptyIcon={<StarBorderIcon fontSize="inherit"/>}
                            />

                        </div>
                        <Button
                            variant={'contained'}
                            color={'primary'}
                            onClick={() => addRev(review)}
                        >
                            Add
                        </Button>
                        <div className='addFileBlock'>
                            <TextField
                                variant="outlined"
                                margin="normal"
                                fullWidth
                                name="image"
                                type="file"
                                placeholder="Image"
                                ref={inputRef}
                                id="image"
                                onChange={fileChangeHandler}
                            />
                            <Button
                                variant={'contained'}
                                color={'primary'}
                                onClick={formSubmit}
                            >
                                Send Image
                            </Button>
                        </div>
                    </div>
                    : null}

            </div>
        );
    }
;

export default SinglePlace;