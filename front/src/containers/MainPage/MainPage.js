import React, {useEffect} from 'react';
import {useSelector, useDispatch} from "react-redux";
import {getPlaces} from "../../store/actions/placeActions";
import PlaceCard from "../../components/PlaceCard/PlaceCard";
import Grid from "@material-ui/core/Grid";
import {makeStyles} from "@material-ui/core/styles";

const useStyles = makeStyles({
    main: {
        display: "flex",
        flexWrap: "wrap",
        justifyContent: "space-between",
        paddingTop: "40px"
    },
});

const MainPage = () => {
    const places = useSelector(state => state.places.places);
    const dispatch = useDispatch();
    const classes = useStyles();

    console.log(places);
    const user = useSelector(state => state.users.user);
    console.log(user);
    useEffect(() => {
        dispatch(getPlaces());
    }, [dispatch]);

    let placesList;

    if (places.length === 0) {
        placesList = (
            <h2>Add first place...</h2>
        );
    } else {
        placesList = places.map(place => {
            let totalFood;
            let totalService;
            let totalInterior;
            let total;

            if (place && place.reviews) {
                totalFood = place.reviews.reduce((prev, next) => prev + next.food, 0);
                totalFood = totalFood / place.reviews.length;
            }
            if (place && place.reviews) {
                totalService = place.reviews.reduce((prev, next) => prev + next.service, 0);
                totalService = totalService / place.reviews.length;
            }
            if (place && place.reviews) {
                totalInterior = place.reviews.reduce((prev, next) => prev + next.interior, 0);
                totalInterior = totalInterior / place.reviews.length;
            }
            if (place && place.reviews) {
                total = (totalFood + totalInterior + totalService) / 3;
            }

            return (
                <PlaceCard
                    value={total ? total : 0}
                    key={place._id}
                    name={place.name}
                    img={place.image}
                    id={place._id}
                    length={place.images.length}
                    reviews={place.reviews.length}
                />
            );
        })
    }

    return (
        <Grid container className={classes.main}>
            {placesList}
        </Grid>
    );
};

export default MainPage;