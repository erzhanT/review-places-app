import React, {useState, useRef} from 'react';
import {useDispatch, useSelector} from "react-redux";
import {addPlace} from "../../store/actions/placeActions";
import Container from "@material-ui/core/Container";
import {makeStyles} from '@material-ui/core/styles';
import TextField from "@material-ui/core/TextField";
import Button from "@material-ui/core/Button";

const useStyles = makeStyles((theme) => ({
    paper: {
        marginTop: theme.spacing(8),
        display: 'flex',
        flexDirection: 'column',
        alignItems: 'center',
    },
    avatar: {
        margin: theme.spacing(1),
        backgroundColor: theme.palette.secondary.main,
    },
    form: {
        width: '100%', // Fix IE 11 issue.
        marginTop: theme.spacing(1),
    },
    submit: {
        margin: theme.spacing(3, 0, 2),
    },
    login: {
        marginLeft: 'auto',
        marginRight: 'auto'
    },
    checkbox: {
        marginTop: '10px'
    }
}));

const AddPlace = () => {
    const classes = useStyles();
    const [place, setPlace] = useState({
        name: '',
        description: '',
        image: '',
        agree: 'false'
    })


    const dispatch = useDispatch();
    const error = useSelector(state => state.places.addPlaceError);

    const inputRef = useRef();

    const fileChangeHandler = e => {
        const name = e.target.name;
        const file = e.target.files[0];
        setPlace(prevState => ({
            ...prevState,
            [name]: file
        }));
    };

    const checked = e => {
        setPlace(prevState => {
            return {...prevState, agree: 'true'};
        });
    };

    const inputChangeHandler = (e) => {
        const name = e.target.name;
        const value = e.target.value;
        setPlace(prevState => {
            return {
                ...prevState,
                [name]: value};
        });
    };
    const formSubmit = (e) => {
        e.preventDefault();
        const formData = new FormData();
        Object.keys(place).forEach(key => {
            formData.append(key, place[key]);
        });
        dispatch(addPlace(formData));
    };

    const getFieldError = fieldName => {
        try {
            return error.errors[fieldName].message;
        } catch (e) {
            return undefined;
        }
    };

    return (
        <Container>
            <form className={classes.form}
                  onSubmit={formSubmit}
            >
                <TextField
                    variant="outlined"
                    margin="normal"
                    error={!!getFieldError("name")}
                    helperText={getFieldError("name")}
                    fullWidth
                    label="Name"
                    name="name"
                    value={place.name}
                    onChange={inputChangeHandler}
                    autoComplete="name"
                    autoFocus
                />
                <TextField
                    variant="outlined"
                    margin="normal"
                    error={!!getFieldError("description")}
                    helperText={getFieldError("description")}
                    fullWidth
                    label="Description"
                    name="description"
                    value={place.description}
                    onChange={inputChangeHandler}
                    autoComplete="description"
                    autoFocus
                />
                <TextField
                    variant="outlined"
                    margin="normal"
                    fullWidth
                    name="image"
                    type="file"
                    placeholder="Image"
                    error={!!getFieldError("image")}
                    helperText={getFieldError("image")}
                    ref={inputRef}
                    id="image"
                    onChange={fileChangeHandler}
                />
                <label htmlFor="agree">
                    <input className={classes.checkbox} type="checkbox" id='agree' name='agree' onChange={checked}/>
                    Agree with terms
                </label>
                <Button
                    type="submit"
                    fullWidth
                    variant="contained"
                    color="primary"
                    className={classes.submit}
                >
                    Add Place
                </Button>
            </form>
        </Container>
    );
};

export default AddPlace;